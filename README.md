# game-of-life-cpp
My game of life implementation in C++.

Uses SFML for async keyboard input (but I plan to rewrite it to be 100% SFML with graphics and stuff).

It loads game state from first argument and saves to a file provided as the second argument.
Second argument is optional.
Example:
```
$ ./a.out gamestate.txt test.txt # loads from gamestate.txt, saves to test.txt
```
## Known issues
[Doesn't work in some terminals](https://gitlab.com/Bravo555/game-of-life-cpp/issues/13), but I'm too lazy to fix it.
