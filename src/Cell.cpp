#include "Cell.hpp"

Cell::Cell(bool isAlive) {
	this->isAlive = isAlive;
}

void Cell::update(const std::vector<std::vector<Cell>> board, int x, int y) {
	int aliveNeighbours = 0;
	for(int i = -1; i <= 1; i++) {
		for(int j = -1; j <= 1; j++) {
			if(!(i == 0 && j == 0) &&
				inBounds(j + x, i + y, board[i + y].size(), board.size()) &&
				board[y + i][x + j].isAlive == true) {
				aliveNeighbours++;
			}
		}
	}

	if(isAlive) {
		if(aliveNeighbours < 2 || aliveNeighbours > 3) {
			isAlive = false;
		}
	}
	else {
		if(aliveNeighbours == 3) {
			isAlive = true;
		}
	}
}

bool Cell::inBounds(int x, int y, int width, int height) {
	return ((x >= 0 && x < width) && (y >= 0 && y < height));
}
