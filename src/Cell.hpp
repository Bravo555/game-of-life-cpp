#include <vector>
#include <iostream>

class Cell {
public:
	bool isAlive;
	
	Cell(bool isAlive);

	void update(const std::vector<std::vector<Cell>> board, int x, int y);

private:
	bool inBounds(int x, int y, int width, int height);

};
