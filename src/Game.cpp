#include "Game.hpp"

#include <chrono>
#include <vector>
#include <fstream>
#include <iostream>

#include <SFML/Window.hpp>

void Game::run() {
	auto lastTime = std::chrono::steady_clock::now();
	bool somethingPressed = false;
	while(!exit) {
		auto current = std::chrono::steady_clock::now();
		auto elapsed = current - lastTime;

		if(!somethingPressed) {
			processInput();
		}
		
		if(elapsed > std::chrono::milliseconds(delay)) {
			clearScreen();
			draw();
			update();
		
			lastTime = current;
		}
	}
}

void Game::loadFromFile(const std::string& filename) {
	std::ifstream file(filename);

	std::string line;
	while(std::getline(file, line)) {
		std::vector<Cell> row;
		for(int j = 0; j < line.size(); j++) {
			
			if(line[j] == '@') {
				row.push_back(Cell(true));
			}
			else {
				row.push_back(Cell(false));
			}
		}
		board.push_back(row);
	}

	file.close();
}

void Game::saveToFile(const std::string& filename) {
	std::ofstream file(filename);
	for(auto& row : board) {
		for(auto& cell : row) {
			if(cell.isAlive) {
				file << '@';
			}
			else {
				file << '-';
			}
		}
		file << '\n';
	}
	file.close();
}

void Game::update() {
	std::vector<std::vector<Cell>> boardCopy = board;

		for (int y = 0; y < board.size(); y++) {
			for (int x = 0; x < board[y].size(); x++) {
				board[y][x].update(boardCopy, x, y);
			}
		}
}

void Game::draw() {
	for(auto& row : board) {
		for (auto &cell : row) {
			if(cell.isAlive) {
				std::cout << '@';
			}
			else {
				std::cout << '.';
			}
		}
		std::cout << '\n';
	}
}

bool Game::processInput() {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
		exit = true;
		return true;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		if(delay > 100) {
			delay -= 100;
			return true;
		}
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		if(delay < 2000) {
			delay += 100;
			return true;
		}
	}
	return false;
}

void Game::clearScreen() {
	printf("\033[H\033[J");
}
