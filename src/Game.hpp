#include <vector>
#include <chrono>
#include "Cell.hpp"

class Game {
private:
	std::vector<std::vector<Cell>> board;
	bool exit = false;
	int delay = 250;

	void update();
	void draw();
	bool processInput();
	void clearScreen();

	public:
	void run();
	void loadFromFile(const std::string& filename);
	void saveToFile(const std::string& filename);
};
