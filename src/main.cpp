#include "Game.hpp"
#include <iostream>

int main(int argc, char** argv) {
    if(argc > 1) {
        Game game;
        std::string initState = argv[1];
        game.loadFromFile(initState);
        game.run();
        if(argc == 3) {
            std::string filename = argv[2];
            game.saveToFile(filename);
        }
    }
    else {
        std::cout << "Please provide an initial state as an argument!\n";
    }
}
